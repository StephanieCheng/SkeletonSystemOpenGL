#version 330 core
out vec4 color;

in vec2 TexCoords;
in vec4 WeightColor;

uniform sampler2D texture_diffuse1;
uniform int renderType;

void main()
{
	switch (renderType){
		case 0: color = vec4(texture(texture_diffuse1, TexCoords)); break;
		case 1:
		case 2: color = WeightColor; break; 
		default: color = vec4(0.0f, 1.0f, 0.0f, 1.0f);
	}
}