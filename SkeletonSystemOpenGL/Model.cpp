#include "Model.h"

#include <iostream>

Model::Model() {

	position[0] = 0.0f;
	position[1] = 0.0f;
	position[2] = 0.0f;
	
	quat_identity(rotation);

	scale[0] = 1.0f;
	scale[1] = 1.0f;
	scale[2] = 1.0f;

	usesSkeleton = false;

	mat4x4_identity(globalInverseTransformation);

	animation = 0;
	animate = false;
	tAnimationStart = Time::now();
	pause = false;
	animateSpecial = -1;

}

Model::~Model() {
}

void Model::LoadAssimp(const std::string &path, bool triangulate) {
	if (triangulate) {
		scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);
	}
	else {
		scene = importer.ReadFile(path, aiProcess_FlipUVs); 
	}
	//scene = importer.ReadFile(path, aiProcess_FlipUVs); //aiProcess_Triangulate |


	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
		std::cout << "ERROR::ASSIMP::" << importer.GetErrorString() << std::endl;
		return;
	}
	else {
		//aiMatrix4x4 GIT;
		//GIT = scene->mRootNode->mTransformation;
		//GIT.Transpose();
		//GIT.Inverse();
		//GIT.Transpose();
		//aiMatTolinMat(globalInverseTransformation, GIT);
		
		directory = path.substr(0, path.find_last_of('/'));
		processNode(scene->mRootNode, scene);

		GetSkeletonBones();
		skeletonMesh.assignToBuffers();


		if (scene->mNumAnimations == 0)
			std::cout << "has no animations! " << std::endl;
		else
			std::cout << "has " << scene->mNumAnimations << " animations" << std::endl;
	}
	
}

void Model::Unload() {

	std::cout << "model unloaded" << std::endl;
	for (GLuint i = 0; i < meshes.size(); i++)
		meshes[i]->Unload();
}

void Model::Draw(Shader& shader)
{
	glEnable(GL_DEPTH_TEST);
	for (GLuint i = 0; i < meshes.size(); i++)
		meshes[i]->Draw(shader);
}

std::vector<Texture> Model::GetLoadedTextures()
{
	return textures_loaded;
}

void Model::SetScale(float x, float y, float z)
{
	scale[0] = x;
	scale[1] = y;
	scale[2] = z;
}

void Model::SetPosition(float x, float y, float z)
{
	position[0] = x;
	position[1] = y;
	position[2] = z;
}

void Model::SetRotation(quat rot)
{
	rotation[0] = rot[0];
	rotation[1] = rot[1];
	rotation[2] = rot[2];
	rotation[3] = rot[3];
}

void Model::GetModelMatrix(mat4x4 model)
{
	CalculateModelMatrix();
	mat4x4_dup(model, modelMatrix);
}

void Model::BoneTransforms(float timeInSeconds, std::vector<MatStruct>& transforms, bool drawArmature)
{
	if (usesSkeleton == false) { return; }


	//needed for animation
	if (pause) {
		//move the start time of the animation
		Time::time_point tnow = Time::now();
		tAnimationStart += (tnow - tAnimationCurrent);
		for (std::map<std::string, AnimationInfo>::iterator it = BoneMasks.begin(); it != BoneMasks.end(); ++it) {
			it->second.tStart = tAnimationStart;
		}
		//update blend animations
		if (animateSpecial != -1) {
			if (transitions.size() > animateSpecial && animateOther == false) {
				transitions[animateSpecial].tAnimationStart = tAnimationStart;
			}
			else if (blends.size() > animateSpecial){
				blends[animateSpecial].tAnimationStart = tAnimationStart;
			}
		}
	}
	tAnimationCurrent  = Time::now();

	//update masks
	for (std::map<std::string, AnimationInfo>::iterator it = BoneMasks.begin(); it != BoneMasks.end(); ++it) {
		it->second.tNow = tAnimationCurrent;
		int animationNum = it->second.animation;

		fsec RunningTime = (it->second.tNow - it->second.tStart);
		timeInSeconds = RunningTime.count();
		float ticksPerSecond = scene->mAnimations[animationNum]->mTicksPerSecond != 0 ?
			scene->mAnimations[animationNum]->mTicksPerSecond : 25.0f;

		float timeInTicks = timeInSeconds * ticksPerSecond;
		//loops the animation
		float animationTimeMask = fmod(timeInTicks, scene->mAnimations[animationNum]->mDuration);
		it->second.animationTime = animationTimeMask;
	}

	if (animateSpecial != -1) {
		if (animateOther == false)
			PlayTransition(timeInSeconds, transforms, drawArmature);
		else
			PlayBlend(timeInSeconds, transforms, drawArmature);

		return;
	}

	fsec RunningTime = (tAnimationCurrent - tAnimationStart);
	timeInSeconds = RunningTime.count();

	mat4x4 I;
	mat4x4_identity(I);
	if (scene->mNumAnimations == 0) {
		return;
		std::cout << "no animations" << std::endl;
	}


	float ticksPerSecond = scene->mAnimations[animation]->mTicksPerSecond != 0 ?
		scene->mAnimations[animation]->mTicksPerSecond : 25.0f;

	float timeInTicks = timeInSeconds * ticksPerSecond;
	//loops the animation
	float animationTime = fmod(timeInTicks, scene->mAnimations[animation]->mDuration);
	//float animationTime = timeInTicks;

	ReadNodeHeirarchy(animationTime, scene->mRootNode, I, animation);
	
	//transforms.resize(numBones);
	for (GLuint i = 0; i < numBones; i++) {
		if (transforms.size() < i + 1) {
			MatStruct mats;
			mat4x4_dup(mats.mat, boneInfoMap[i].finalTransformation);
			transforms.push_back(mats);
			//transforms.push_back(boneInfoMap[i].finalTransformation);
		}
		else
		{
			mat4x4_dup(transforms[i].mat, boneInfoMap[i].finalTransformation);
		}
	}

}

GLuint Model::UsesSkeleton()
{
	return usesSkeleton;
}

void Model::printMeshBones()
{
	std::cout << "Num meshes " << meshes.size() << std::endl;
	std::cout << "Num bones " << boneMap.size() << std::endl;
	std::cout << "Num vertices" << scene->mMeshes[0]->mNumVertices << std::endl;
	for (int i = 0; i < meshes.size(); i++) {
		meshes.at(i)->printVertexBoneData();
	}
}

void Model::PlayAnimation(std::string animName) {
	//name is like "rig|nameofanimation", we cut out the rig|
	int anim = FindAnimationIndex(animName);

	if (anim == -1) {
		std::cout << "no such animation" << std::endl;
		return;
	}
	tAnimationStart = Time::now();
	animation = anim;
	std::cout << "Playing animation " << animName << std::endl;
	return;
}

void Model::PlayNextAnimation()
{
	if (animate == false) {
		animation = 0;
		animate = true;
		tAnimationStart = Time::now();
	}
	else {
		tAnimationStart = Time::now();
		animation = fmod(animation + 1, scene->mNumAnimations);
	}
	std::string name = scene->mAnimations[animation]->mName.data;
	name = name.substr(name.find("|") + 1);
	std::cout << "Playing animation " << name << std::endl;

}
void Model::StopAnimation()
{
	animate = false;
}
void Model::PauseAnimation() {
	pause = true;
}
void Model::UnpauseAnimation() {
	pause = false;
}
void Model::TogglePlayPause() {
	pause = !pause;
	if(pause) std::cout << "Paused" << std::endl;
	else std::cout << "Unpaused" << std::endl;
}

std::chrono::milliseconds Model::GetAnimationLengthOfOneFrame() {
	float ticksPerSecond = scene->mAnimations[animation]->mTicksPerSecond != 0 ?
		scene->mAnimations[animation]->mTicksPerSecond : 25.0f;

	int timeinMiliseconds = (1.0f / ticksPerSecond) * 1000;
	//ticks per second -> frames per second of animation (irrelevant of real framerate)
	//std::chrono::seconds(timeInSeconds);
	//we need to move 1 tick
	//tAnimationCurrent += std::chrono::duration_cast<std::chrono::high_resolution_clock::duration>(std::chrono::milliseconds(timeinMiliseconds));
	return std::chrono::milliseconds(timeinMiliseconds);
}
// will pause the animation
void Model::NextFrame() {
	PauseAnimation();
	
	tAnimationCurrent += GetAnimationLengthOfOneFrame();
}
void Model::PreviousFrame() {
	PauseAnimation();
	tAnimationCurrent -= GetAnimationLengthOfOneFrame();
}

// will pause the animation
void Model::ForwardTenFrames() {
	PauseAnimation();
	tAnimationCurrent += GetAnimationLengthOfOneFrame() * 10;
}
void Model::BackwardTenFrames() {
	PauseAnimation();

	tAnimationCurrent -= GetAnimationLengthOfOneFrame() * 10;
}

void Model::DrawArmature()
{
	if (usesSkeleton == false) { return; }
	skeletonMesh.Draw();
}

int Model::FindAnimationIndex(std::string animName)
{
	for (int i = 0; i < scene->mNumAnimations; i++) {
		std::string name = scene->mAnimations[i]->mName.data;
		name = name.substr(name.find("|")+1);
		if (name == animName) {
			return i;
		}
	}
	return -1;
}

void Model::SetMaskAndAnimation(std::string BoneName, std::string AnimationName)
{
	int anim = FindAnimationIndex(AnimationName);
	if (anim == -1) { std::cout << "no such animation exist" << std::endl; return; }
	AnimationInfo ainfo;
	ainfo.animation = anim;
	ainfo.tStart = Time::now();
	ainfo.tNow = Time::now();
	BoneMasks[BoneName] = ainfo;
}

void Model::ClearMasks()
{
	BoneMasks.clear();
}

int Model::SelectBone(std::string boneName)
{
	return boneMap[boneName];
}
void Model::AddBlendWeight(bool sub) {
	if (animateSpecial != -1 && blends.size() > animateSpecial) {
		float weight = blends[animateSpecial].weight;
		if (sub) {
			weight -= 0.01;
		}
		else {
			weight += 0.01;
		}
		if (weight > 1.0) weight = 1.0f;
		if (weight < 0.0) weight = 0.0f;
		blends[animateSpecial].weight = weight;
		std::cout << "weight: " << weight << std::endl;
	}
	
}

void Model::MakeBlend(int iAnim1, int iAnim2, float weight)
{
	BlendInfo bi;
	bi.anim1 = iAnim1;
	bi.anim2 = iAnim2;
	bi.weight = weight;

	bi.tAnimationStart = Time::now();
	bi.tAnimationCurrent = bi.tAnimationStart;

	blends.push_back(bi);

	SetPlayBlend();
	animateSpecial = blends.size() - 1;
}

void Model::MakeTransitions(int iAnim1, int iAnim2, int frameStart, int frameEnd)
{
	
	TransitionInfo bi;
	bi.frameStart = frameStart;
	bi.frameEnd = frameEnd;
	bi.anim1 = iAnim1;
	bi.anim2 = iAnim2;
	bi.tAnimationStart = Time::now();
	bi.tAnimationCurrent = bi.tAnimationStart;

	//blend is defined by the two animatons its blending and the transition consisting of start and end times
	transitions.push_back(bi);

	SetPlayTransition();
	animateSpecial = transitions.size() - 1;
}
void Model::TogglePlayAnimBlend() {
	if (animateSpecial == -1) {
		animateSpecial = 0;
		animate = true;
		std::cout << "Playing Blend 0" << std::endl;
	}
	else {
		animateSpecial = -1;
		animate = false;
		std::cout << "Play Blend Off" << std::endl;
	}
}

void Model::ToggleTransitionsOrBlends() {
	animateOther = !animateOther;
	animateSpecial = 0;
	animate = true;
}

void Model::PlayAnimBlend(int i) {
	if (animateOther) {
		if (blends.size() <= i) {
			std::cout << "No Blend at this index" << std::endl;
			return;
		}
	}
	else if (transitions.size() <= i) {
		std::cout << "No Transitions at this index" << std::endl;
		return;
	}
	animateSpecial = i;
	animate = true;
	pause = false;
	std::cout << "Playing " << i << std::endl;
}

void Model::TurnOffPlayBlend() {
	animateSpecial = -1;
	animate = false;
}
void Model::PlayNextBlend() {
	if (animateOther) {
		animateSpecial = (animateSpecial + 1) % blends.size();
	}
	else {
		animateSpecial = (animateSpecial + 1) % transitions.size();
	}
	animate = true;
	std::cout << "Playing " << animateSpecial << std::endl;
}

void Model::SetPlayTransition()
{
	animateOther = false;
	animate = true;
	animateSpecial = 0;
	pause = false;
}

void Model::SetPlayBlend()
{
	animateOther = true;
	animate = true;
	animateSpecial = 0;
	pause = false;
}

//temp, all the play funcitons should be unified!
void Model::PlayBlend(float timeInSeconds, std::vector<MatStruct>& transforms, bool drawArmatures){
	if (blends.size() == 0) {
		std::cerr << "there are no animaiton blends, make one through the console" << std::endl;
		animateSpecial = -1;
		animate = 0;
		return;
	}

	int anim1 = blends[animateSpecial].anim1;
	int anim2 = blends[animateSpecial].anim2;

	blends[animateSpecial].tAnimationCurrent = Time::now();

	fsec RunningTime = (blends[animateSpecial].tAnimationCurrent - blends[animateSpecial].tAnimationStart);
	timeInSeconds = RunningTime.count();

	//animaitons should have the same ticks per second
	//char animation is 24fps
	float ticksPerSecond1 = scene->mAnimations[anim1]->mTicksPerSecond != 0 ?
		scene->mAnimations[anim1]->mTicksPerSecond : 25.0f;

	float ticksPerSecond2 = scene->mAnimations[anim2]->mTicksPerSecond != 0 ?
		scene->mAnimations[anim2]->mTicksPerSecond : 25.0f;


	float timeInTicks1 = timeInSeconds * ticksPerSecond1;
	//loops the animation
	float animationTime1 = fmod(timeInTicks1, scene->mAnimations[anim1]->mDuration);
	//float animationTime = timeInTicks;

	float timeInTicks2 = timeInSeconds * ticksPerSecond2;
	//loops the animation
	float animationTime2 = fmod(timeInTicks2, scene->mAnimations[anim2]->mDuration);
	//float animationTime = timeInTicks;

	mat4x4 I;
	mat4x4_identity(I);

	MakeBlend(animationTime1, animationTime2, scene->mRootNode, I, anim1, anim2, blends[animateSpecial].weight);

	GetFinalBoneTransforms(transforms);
}

void Model::PlayTransition(float timeInSeconds, std::vector<MatStruct>& transforms, bool drawArmatures) {

	if (transitions.size() == 0) {
		std::cerr << "there are no animaiton blends, make one through the console" << std::endl;
		animateSpecial = -1;
		animate = 0;
		return;
	}

	int anim1 = transitions[animateSpecial].anim1;
	int anim2 = transitions[animateSpecial].anim2;

	int frameStart = transitions[animateSpecial].frameStart;
	int frameEnd = transitions[animateSpecial].frameEnd;

	transitions[animateSpecial].tAnimationCurrent = Time::now();

	fsec RunningTime = (transitions[animateSpecial].tAnimationCurrent - transitions[animateSpecial].tAnimationStart);
	timeInSeconds = RunningTime.count();

	//animaitons should have the same ticks per second
	//char animation is 24fps
	float ticksPerSecond1 = scene->mAnimations[anim1]->mTicksPerSecond != 0 ?
		scene->mAnimations[anim1]->mTicksPerSecond : 25.0f;

	float ticksPerSecond2 = scene->mAnimations[anim2]->mTicksPerSecond != 0 ?
		scene->mAnimations[anim2]->mTicksPerSecond : 25.0f;

	float transitionInTicks = frameEnd - frameStart;

	//animaitons should have the same ticks per second
	float ticksPerSecond = ticksPerSecond1;

	//total ticks per second aka duration is anim1 + anim2 - transition;
	float duraiton = scene->mAnimations[anim1]->mDuration + scene->mAnimations[anim2]->mDuration - (transitionInTicks);

	float timeInTicks = timeInSeconds * ticksPerSecond;
	//loops the animation
	float animationTime = fmod(timeInTicks, duraiton);
	//float animationTime = timeInTicks;

	mat4x4 I;
	mat4x4_identity(I);

	// outside of transition only one of the animaitons is played
	if (animationTime < frameStart) {
		ReadNodeHeirarchy(animationTime, scene->mRootNode, I, anim1);
	}
	// inside the transition the animations should be Blended!
	// animate true prevents a needless blend of the bind pose
	else if(animationTime < frameEnd && animate == true){
		
		//0 to 1, depending on the time inbetween transitions
		float factor = (animationTime - frameStart) / transitionInTicks;

		MakeBlend(animationTime, animationTime - frameStart, scene->mRootNode, I, anim1, anim2, factor);

		
	}
	//after frameEnd the second animation plays normally
	else {
		//the second animations starts where the transition starts
		ReadNodeHeirarchy(animationTime - frameStart, scene->mRootNode, I, anim2);
	}

	GetFinalBoneTransforms(transforms);

}


void Model::GetFinalBoneTransforms(std::vector<MatStruct>& transforms) {
	for (GLuint i = 0; i < numBones; i++) {
		if (transforms.size() < i + 1) {
			MatStruct mats;
			mat4x4_dup(mats.mat, boneInfoMap[i].finalTransformation);
			transforms.push_back(mats);
			//transforms.push_back(boneInfoMap[i].finalTransformation);
		}
		else
		{
			mat4x4_dup(transforms[i].mat, boneInfoMap[i].finalTransformation);
		}
	}
}

void Model::processNode(aiNode * node, const aiScene * scene)
{
	for (GLuint i = 0; i < node->mNumMeshes; i++) {
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		this->meshes.push_back(this->processMesh(mesh, scene));
	}//mesh destroyed ??
	
	for (GLuint i = 0; i < node->mNumChildren; i++) {
		processNode(node->mChildren[i], scene);
	}
}

std::unique_ptr<Mesh> Model::processMesh(const aiMesh * mesh, const aiScene * scene)
{
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;
	std::vector<Texture> textures;

	for (GLuint i = 0; i < mesh->mNumVertices; i++) {
		Vertex vertex;
		
		vec3 vector;
		vector[0] = mesh->mVertices[i].x;
		vector[1] = mesh->mVertices[i].y;
		vector[2] = mesh->mVertices[i].z;

		vertex.position[0] = vector[0];
		vertex.position[1] = vector[1];
		vertex.position[2] = vector[2];

		vec3 vector2;
		vector2[0] = mesh->mNormals[i].x;
		vector2[1] = mesh->mNormals[i].y;
		vector2[2] = mesh->mNormals[i].z;

		vertex.normal[0] = vector2[0];
		vertex.normal[1] = vector2[1];
		vertex.normal[2] = vector2[2];

		vec2 vec;
		vec[0] = 0;
		vec[1] = 0;

		if (mesh->mTextureCoords[0]) {
			vec[0] = mesh->mTextureCoords[0][i].x;
			vec[1] = mesh->mTextureCoords[0][i].y;

		}
		vertex.texcoord[0] = vec[0];
		vertex.texcoord[1] = vec[1];


		vertices.push_back(vertex);

	}
	//process indices

	for (GLuint i = 0; i < mesh->mNumFaces; i++) {
		aiFace face = mesh->mFaces[i];
		for (GLuint j = 0; j < face.mNumIndices; j++) {
			indices.push_back(face.mIndices[j]);
		}
	}

	//proces materials
	if (mesh->mMaterialIndex >= 0) {
		aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
		std::vector<Texture> diffuseMaps = this->loadMaterialTextures(material,
			aiTextureType_DIFFUSE, "texture_diffuse");
		textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());

		std::vector<Texture> specularMaps = this->loadMaterialTextures(material,
			aiTextureType_SPECULAR, "texture_specular");
	}
	// must be delted by the model destructor
	std::unique_ptr<Mesh> createdMesh = std::make_unique<Mesh>();

	//Mesh createdMesh = Mesh::Mesh();
	createdMesh->setupMesh(vertices, indices, textures);

	//process bones
	//std::cout << "bones " << mesh->mNumBones << std::endl;

	if (mesh->mNumBones != 0) {
		usesSkeleton = true;

		for (int i = 0; i < mesh->mNumBones; i++) {
			int boneIndex;
			std::string boneName(mesh->mBones[i]->mName.data);

			if (boneMap.find(boneName) == boneMap.end()) {
				boneIndex = numBones;
				numBones++;
				BoneInfo bi;
				mat4x4_identity(bi.boneOffset);
				mat4x4_identity(bi.finalTransformation);
				if(boneInfoMap.find(boneIndex) != boneInfoMap.end())
					boneInfoMap.at(boneIndex) = bi;
				else
				{
					boneInfoMap.insert(std::make_pair(boneIndex, bi));
				}
			}
			else {
				boneIndex = boneMap[boneName];
			}

			boneMap[boneName] = boneIndex;
			//mat4x4_dup(boneInfoMap[boneIndex].boneOffset, mesh->mBones[i]->mOffsetMatrix);

			aiMatrix4x4 aiMat = mesh->mBones[i]->mOffsetMatrix;
			aiMatTolinMat(boneInfoMap[boneIndex].boneOffset, aiMat);

			for (int j = 0; j < mesh->mBones[i]->mNumWeights; j++) {
				int vertexID = mesh->mBones[i]->mWeights[j].mVertexId;
				float Weight = mesh->mBones[i]->mWeights[j].mWeight;
			
				if (Weight <= 0.1) continue;

				//std::cout << "bone assigned weigt " << Weight << std::endl;
				createdMesh->AddBoneData(boneIndex, Weight, vertexID);
			}
			// save animation keyframes for this bone
			for (int anim = 0; anim < scene->mNumAnimations; ++anim) {
				const aiAnimation* pAnimation = scene->mAnimations[anim];
				std::string animName = pAnimation->mName.data;
				for (int i = 0; i < pAnimation->mNumChannels; i++) {
					//save each chanell to a bone
					if (pAnimation->mChannels[i]->mNodeName.data == boneName) {
						animations[animName][boneName] = pAnimation->mChannels[i];
						break;
					}
				}
			}
		}
		createdMesh->assignSkeleton();
		

	}

	return createdMesh;
}

std::vector<Texture> Model::loadMaterialTextures(aiMaterial * mat, aiTextureType type, std::string typeName)
{
	std::vector<Texture> textures;
	for (GLuint i = 0; i < mat->GetTextureCount(type); i++) {
		aiString str;
		mat->GetTexture(type, i, &str);
		GLboolean skip = false;
		for (GLuint j = 0; j < textures_loaded.size(); j++) {
			if (std::strcmp(textures_loaded[j].GetPath().C_Str(), str.C_Str()) == 0) {
				textures.push_back(textures_loaded[j]);
				skip = true;
				break;
			}
		}
		if (!skip) {
			Texture texture = Texture();
			texture.LoadTextureFromDirectory(str.C_Str(), this->directory);
			texture.SetType(typeName);
			texture.SetPath(str);
			textures.push_back(texture);
			textures_loaded.push_back(texture);
		}
	}
	return textures;
}
void Model::CalculateModelMatrix()
{
	//mat4x4 I;
	mat4x4_identity(modelMatrix);
	//mat4x4_identity(I);

	mat4x4_translate(modelMatrix, position[0], position[1], position[2]);
	mat4x4_scale_aniso(modelMatrix, modelMatrix, scale[0], scale[1], scale[2]);
	//mat4x4o_mul_quat(modelMatrix, I, rotation); needs to be orthogonal??

}

void Model::aiMatTolinMat(mat4x4 mat1, aiMatrix4x4 mat2)
{
	//mat2 = mat2.Transpose();
	
	/*mat1[0][0] = mat2[0][0];
	mat1[0][1] = mat2[0][1];
	mat1[0][2] = mat2[0][2];
	mat1[0][3] = mat2[0][3];

	mat1[1][0] = mat2[1][0];
	mat1[1][1] = mat2[1][1];
	mat1[1][2] = mat2[1][2];
	mat1[1][3] = mat2[1][3];

	mat1[2][0] = mat2[2][0];
	mat1[2][1] = mat2[2][1];
	mat1[2][2] = mat2[2][2];
	mat1[2][3] = mat2[2][3];

	mat1[3][0] = mat2[3][0];
	mat1[3][1] = mat2[3][1];
	mat1[3][2] = mat2[3][2];
	mat1[3][3] = mat2[3][3];*/

	mat1[0][0] = mat2[0][0];
	mat1[0][1] = mat2[1][0];
	mat1[0][2] = mat2[2][0];
	mat1[0][3] = mat2[3][0];

	mat1[1][0] = mat2[0][1];
	mat1[1][1] = mat2[1][1];
	mat1[1][2] = mat2[2][1];
	mat1[1][3] = mat2[3][1];

	mat1[2][0] = mat2[0][2];
	mat1[2][1] = mat2[1][2];
	mat1[2][2] = mat2[2][2];
	mat1[2][3] = mat2[3][2];

	mat1[3][0] = mat2[0][3];
	mat1[3][1] = mat2[1][3];
	mat1[3][2] = mat2[2][3];
	mat1[3][3] = mat2[3][3];


}

void Model::MakeBlend(float animationTime, float animationTime2, const aiNode * pNode, mat4x4 parentTransform, int animation, int animation2, float factor) {
	{
		std::string NodeName(pNode->mName.data);

		int animationBuffer = animation;
		float animationTimeBuffer = animationTime;
		int animationBuffer2 = animation2;
		float animationTimeBuffer2 = animationTime2;

		aiAnimation* pAnimation = scene->mAnimations[animation];
		aiAnimation* pAnimation2 = scene->mAnimations[animation2];

		mat4x4 nodeTransformation;
		mat4x4_identity(nodeTransformation);
		aiMatTolinMat(nodeTransformation, pNode->mTransformation);

		const aiNodeAnim* pNodeAnim = nullptr;
		const aiNodeAnim* pNodeAnim2 = nullptr;

		pNodeAnim = animations[pAnimation->mName.data][NodeName];
		pNodeAnim2 = animations[pAnimation2->mName.data][NodeName];

		if (BoneMasks.find(NodeName) != BoneMasks.end()) { // mask starts here
			pAnimation = scene->mAnimations[BoneMasks[NodeName].animation];
			//switch out the animation
			animation = BoneMasks[NodeName].animation;
			animationTime = BoneMasks[NodeName].animationTime;
			animation2 = animation;
		}
		//what if one of the animations doesnt exits for this bone?
		if (pNodeAnim && pNodeAnim2 && animate == true) {

			//interpolate scaling and generate scaling transformaion matrix
			aiVector3D scaling1;
			CalcInterpolatedScaling(scaling1, animationTime, pNodeAnim);

			aiVector3D scaling2;
			CalcInterpolatedScaling(scaling2, animationTime2, pNodeAnim2);

			aiVector3D scaling;
			//lin interpoalte scaling
			scaling = scaling1 * (1.0f - factor) + scaling2 * factor;

			mat4x4 scalingM;
			mat4x4_identity(scalingM);
			mat4x4_scale_aniso(scalingM, scalingM, scaling[0], scaling[1], scaling[2]);

			//interpolate rotation and generate scaling transfomration matrix

			aiQuaternion rotationQ1;
			CalcInterpolatedRotation(rotationQ1, animationTime, pNodeAnim);

			aiQuaternion rotationQ2;
			CalcInterpolatedRotation(rotationQ2, animationTime2, pNodeAnim2);

			aiQuaternion rotationQ;
			aiQuaternion::Interpolate(rotationQ, rotationQ1, rotationQ2, factor);
			rotationQ = rotationQ.Normalize();


			aiMatrix3x3 rotationAiMat = rotationQ.GetMatrix();
			mat4x4 rotationM;
			mat4x4_identity(rotationM);
			quat rotQuat;
			rotQuat[0] = rotationQ.x;
			rotQuat[1] = rotationQ.y;
			rotQuat[2] = rotationQ.z;
			rotQuat[3] = rotationQ.w;
			//quat_norm(rotQuat, rotQuat);
			mat4x4_from_quat(rotationM, rotQuat);


			//interpolate translation and generate translation transformation matrix
			aiVector3D translation1;
			CalcIntrepolatedPosition(translation1, animationTime, pNodeAnim);

			aiVector3D translation2;
			CalcIntrepolatedPosition(translation2, animationTime2, pNodeAnim2);

			aiVector3D translation;
			translation = translation1 * (1.0f - factor) + translation2 * factor;

			mat4x4 translationM;
			mat4x4_identity(translationM);
			mat4x4_translate(translationM, translation.x, translation.y, translation.z);


			mat4x4_scale_aniso(translationM, translationM, scaling[0], scaling[1], scaling[2]);
			mat4x4_mul(nodeTransformation, translationM, rotationM);

			//mat4x4_dup(nodeTransformation, rotationM);

		}
		mat4x4 globalTransformation;
		mat4x4_mul(globalTransformation, parentTransform, nodeTransformation);

		if (boneMap.find(NodeName) != boneMap.end()) {
			GLuint boneIndex = boneMap[NodeName];
			mat4x4 res1;
			mat4x4_mul(res1, globalTransformation, boneInfoMap[boneIndex].boneOffset);
			mat4x4_mul(boneInfoMap[boneIndex].finalTransformation, globalInverseTransformation, res1);
		}

		for (GLuint i = 0; i < pNode->mNumChildren; i++) {
			MakeBlend(animationTime, animationTime2, pNode->mChildren[i], globalTransformation, animation, animation2, factor);
		}
		animation = animationBuffer;
		animationTime = animationTimeBuffer;
		animation2 = animationBuffer2;
		animationTime2 = animationTimeBuffer2;
	}
}

void Model::ReadNodeHeirarchy(float animationTime, const aiNode * pNode, mat4x4 parentTransform, int animation)
{
	std::string NodeName(pNode->mName.data);

	int animationBuffer = animation;
	float animationTimeBuffer = animationTime;

	aiAnimation* pAnimation = scene->mAnimations[animation];

	mat4x4 nodeTransformation;
	mat4x4_identity(nodeTransformation);
	aiMatTolinMat(nodeTransformation, pNode->mTransformation);

	const aiNodeAnim* pNodeAnim = nullptr;

	pNodeAnim = animations[pAnimation->mName.data][NodeName];

	if (BoneMasks.find(NodeName) != BoneMasks.end()) { // mask starts here
		pAnimation = scene->mAnimations[BoneMasks[NodeName].animation];
		//switch out the animation
		animation = BoneMasks[NodeName].animation;
		animationTime = BoneMasks[NodeName].animationTime;
	}

	//if this node is an animated bone
	if (pNodeAnim && animate == true) {

		//interpolate scaling and generate scaling transformaion matrix
		aiVector3D scaling;
		CalcInterpolatedScaling(scaling, animationTime, pNodeAnim);

		mat4x4 scalingM;
		mat4x4_identity(scalingM);
		mat4x4_scale_aniso(scalingM, scalingM, scaling[0], scaling[1], scaling[2]);

		//interpolate rotation and generate scaling transfomration matrix

		aiQuaternion rotationQ;
		CalcInterpolatedRotation(rotationQ, animationTime, pNodeAnim);
		aiMatrix3x3 rotationAiMat = rotationQ.GetMatrix();
		mat4x4 rotationM;
		mat4x4_identity(rotationM);
		quat rotQuat;
		rotQuat[0] = rotationQ.x;
		rotQuat[1] = rotationQ.y;
		rotQuat[2] = rotationQ.z;
		rotQuat[3] = rotationQ.w;
		//quat_norm(rotQuat, rotQuat);
		mat4x4_from_quat(rotationM, rotQuat);


		//interpolate translation and generate translation transformation matrix
		aiVector3D translation;
		CalcIntrepolatedPosition(translation, animationTime, pNodeAnim);
		mat4x4 translationM;
		mat4x4_identity(translationM);
		mat4x4_translate(translationM, translation.x, translation.y, translation.z);


		mat4x4_scale_aniso(translationM, translationM, scaling[0], scaling[1], scaling[2]);
		mat4x4_mul(nodeTransformation, translationM, rotationM);

		//mat4x4_dup(nodeTransformation, rotationM);

	}
	mat4x4 globalTransformation;
	mat4x4_mul(globalTransformation, parentTransform, nodeTransformation);


	if (boneMap.find(NodeName) != boneMap.end()) {
		GLuint boneIndex = boneMap[NodeName];
		mat4x4 res1;
		mat4x4_mul(res1, globalTransformation, boneInfoMap[boneIndex].boneOffset);
		mat4x4_mul(boneInfoMap[boneIndex].finalTransformation, globalInverseTransformation, res1);
	}

	for (GLuint i = 0; i < pNode->mNumChildren; i++) {
		ReadNodeHeirarchy(animationTime, pNode->mChildren[i], globalTransformation, animation);
	}
	animation = animationBuffer;
	animationTime = animationTimeBuffer;
}

void Model::CalcInterpolatedRotation(aiQuaternion & out, float animationTime, const aiNodeAnim * pNodeAnim)
{
	if (pNodeAnim->mNumRotationKeys == 0) {
		out.x = 0.0f;
		out.y = 0.0f;
		out.z = 0.0f;
		out.w = 1.0f;
		return;
	}

	//we need at least two values to interpolate
	if (pNodeAnim->mNumRotationKeys == 1) {
		out = pNodeAnim->mRotationKeys[0].mValue;
		out = out.Normalize();
		return;
	}

	if (animationTime <= pNodeAnim->mRotationKeys[0].mTime) {
		out = pNodeAnim->mRotationKeys[0].mValue;
		out = out.Normalize();
		return;
	}
	if (animationTime >= pNodeAnim->mRotationKeys[pNodeAnim->mNumRotationKeys - 1].mTime) {
		out = pNodeAnim->mRotationKeys[pNodeAnim->mNumRotationKeys - 1].mValue;
		out = out.Normalize();
		return;
	}

	GLuint rotationIndex = FindRotation(animationTime, pNodeAnim);
	GLuint nextRotationIndex = (rotationIndex + 1);
	assert(nextRotationIndex < pNodeAnim->mNumRotationKeys);
	float deltaTime = pNodeAnim->mRotationKeys[nextRotationIndex].mTime - pNodeAnim->mRotationKeys[rotationIndex].mTime;

	float factor = (animationTime - (float)pNodeAnim->mRotationKeys[rotationIndex].mTime) / 
		((float)pNodeAnim->mRotationKeys[nextRotationIndex].mTime - (float)pNodeAnim->mRotationKeys[rotationIndex].mTime);
	//float factor = (animationTime - (float)pNodeAnim->mRotationKeys[rotationIndex].mTime);

	assert(factor >= 0.0f && factor <= 1.0f);

	const aiQuaternion& startRotationQ = pNodeAnim->mRotationKeys[rotationIndex].mValue;

	const aiQuaternion& endRotationQ = pNodeAnim->mRotationKeys[nextRotationIndex].mValue;

	aiQuaternion::Interpolate(out, startRotationQ, endRotationQ, factor);
	out = out.Normalize();
}

void Model::CalcInterpolatedScaling(aiVector3D & out, float animationTime, const aiNodeAnim * pNodeAnim)
{

	if (pNodeAnim->mNumScalingKeys == 0) {
		out.x = 1.0f;
		out.y = 1.0f;
		out.z = 1.0f;
		return;
	}
	if (pNodeAnim->mNumScalingKeys == 1) {
		out = pNodeAnim->mScalingKeys[0].mValue;
		return;
	}

	if (animationTime <= pNodeAnim->mScalingKeys[0].mTime) {
		out = pNodeAnim->mScalingKeys[0].mValue;
		return;
	}


	if (animationTime >= pNodeAnim->mScalingKeys[pNodeAnim->mNumScalingKeys - 1].mTime) {
		out = pNodeAnim->mScalingKeys[pNodeAnim->mNumScalingKeys - 1].mValue;
		return;
	}

	GLuint scalingIndex = FindScale(animationTime, pNodeAnim);
	GLuint nextScalingIndex = (scalingIndex + 1);
	assert(nextScalingIndex < pNodeAnim->mNumScalingKeys);

	/*float factor = (animationTime - (float)pNodeAnim->mScalingKeys[scalingIndex].mTime) /
		((float)pNodeAnim->mScalingKeys[nextScalingIndex].mTime - (float)pNodeAnim->mScalingKeys[scalingIndex].mTime);*/


	// logarithmic interpolation, zero time is unsupported
	/*float factor = (logf(animationTime) - logf((float)pNodeAnim->mScalingKeys[scalingIndex].mTime)) /
		(logf((float)pNodeAnim->mScalingKeys[nextScalingIndex].mTime) - logf((float)pNodeAnim->mScalingKeys[scalingIndex].mTime));*/

	//lin
	float factor = (animationTime - (float)pNodeAnim->mScalingKeys[scalingIndex].mTime) /
		((float)pNodeAnim->mScalingKeys[nextScalingIndex].mTime - (float)pNodeAnim->mScalingKeys[scalingIndex].mTime);


	assert(factor >= 0.0f && factor <= 1.0f);

	const aiVector3D& startScale = pNodeAnim->mScalingKeys[scalingIndex].mValue;

	const aiVector3D& endScale = pNodeAnim->mScalingKeys[nextScalingIndex].mValue;

	//logarithmic Interpolaiton LERP 
	out = aiVector3D(startScale.x * (1.0f - factor) + endScale.x * factor,
		startScale.y * (1.0f - factor) + endScale.y * factor,
		startScale.z * (1.0f - factor) + endScale.z * factor);
}

void Model::CalcIntrepolatedPosition(aiVector3D & out, float animationTime, const aiNodeAnim * pNodeAnim)
{
	if (pNodeAnim->mNumPositionKeys == 0) {
		out.x = 0.0f;
		out.y = 0.0f;
		out.z = 0.0f;
		return;
	}

	if (pNodeAnim->mNumPositionKeys == 1) {
		out = pNodeAnim->mPositionKeys[0].mValue;
		return;
	}

	if (animationTime <= pNodeAnim->mPositionKeys[0].mTime) {
		out = pNodeAnim->mPositionKeys[0].mValue;
		return;
	}


	if (animationTime >= pNodeAnim->mPositionKeys[pNodeAnim->mNumPositionKeys - 1].mTime) {
		out = pNodeAnim->mPositionKeys[pNodeAnim->mNumPositionKeys - 1].mValue;
		return;
	}

	GLuint positionIndex = FindPosition(animationTime, pNodeAnim);
	GLuint nextPositionIndex = (positionIndex + 1);
	assert(nextPositionIndex < pNodeAnim->mNumPositionKeys);

	float factor = (animationTime - (float)pNodeAnim->mPositionKeys[positionIndex].mTime) /
		((float)pNodeAnim->mPositionKeys[nextPositionIndex].mTime - (float)pNodeAnim->mPositionKeys[positionIndex].mTime);


	assert(factor >= 0.0f && factor <= 1.0f);

	const aiVector3D& startPosition = pNodeAnim->mPositionKeys[positionIndex].mValue;

	const aiVector3D& endPosition = pNodeAnim->mPositionKeys[nextPositionIndex].mValue;

	//linearInterpolaiton LERP 
	out = startPosition * (1.0f - factor) + endPosition * factor;
}

GLuint Model::FindRotation(float animationTime, const aiNodeAnim * pNodeAnim)
{
	assert(pNodeAnim->mNumRotationKeys > 0);

	for (GLuint i = 0; i < pNodeAnim->mNumRotationKeys; i++) {
		if (animationTime < (float)pNodeAnim->mRotationKeys[i + 1].mTime) {
			return i;
		}
	}
	assert(0);
}

GLuint Model::FindPosition(float animationTime, const aiNodeAnim * pNodeAnim)
{
	assert(pNodeAnim->mNumPositionKeys > 0);

	for (GLuint i = 0; i < pNodeAnim->mNumPositionKeys; i++) {
		if (animationTime < (float)pNodeAnim->mPositionKeys[i + 1].mTime) {
			return i;
		}
	}
	assert(0);
}

GLuint Model::FindScale(float animationTime, const aiNodeAnim * pNodeAnim)
{
	assert(pNodeAnim->mNumScalingKeys > 0);

	for (GLuint i = 0; i < pNodeAnim->mNumScalingKeys; i++) {
		if (animationTime < (float)pNodeAnim->mScalingKeys[i + 1].mTime) {
			return i;
		}
	}
	assert(0);
}

void Model::GetSkeletonBones() {
	mat4x4 I;
	mat4x4_identity(I);
	vec3 pos;
	aiNode* pNode = scene->mRootNode;

	mat4x4 nodeTransformation;
	aiMatTolinMat(nodeTransformation, pNode->mTransformation);
	//pAnimation->mChannels[0]->mNodeName

	pos[0] = nodeTransformation[3][0];
	pos[1] = nodeTransformation[3][1];
	pos[2] = nodeTransformation[3][2];

	//GetSkeletonBonesRec(pNode, I, pos);

	for (GLuint i = 0; i < pNode->mNumChildren; i++) {
		GetSkeletonBonesRec(pNode->mChildren[i], I, pos, -1, false);
	}
}

void Model::GetSkeletonBonesRec(aiNode* pNode, mat4x4 parentTransform, vec3 startPosition, int prevID, bool recThroughSkeleton)
{
	//bones are joints, we draw lines that connect them - we need previous position and this position
	std::string NodeName(pNode->mName.data);


	mat4x4 nodeTransformation;
	aiMatTolinMat(nodeTransformation, pNode->mTransformation);
	//pAnimation->mChannels[0]->mNodeName

	//mat4x4 res1;
	mat4x4 res;
	vec3 pos;
	pos[0] = startPosition[0];
	pos[1] = startPosition[1];
	pos[2] = startPosition[2];


	mat4x4_mul(res, parentTransform, nodeTransformation);

	int ID = -1;

	//const aiNodeAnim* pNodeAnim = nullptr;

	//hotfix for leafbones from blender
	std::string cName = NodeName.substr(NodeName.find_last_of("_") + 1);

	if (boneMap.find(NodeName) != boneMap.end() || cName == "end") {
		if (cName == "end") {
			ID = prevID;
		}
		else {
			ID = boneMap[NodeName];
		}
		recThroughSkeleton = true;

		pos[0] = res[3][0];
		pos[1] = res[3][1];
		pos[2] = res[3][2];


		skeletonMesh.AddBoneData(prevID, ID, startPosition, pos);
	}

	if (recThroughSkeleton) {
		for (GLuint i = 0; i < pNode->mNumChildren; i++) {
			GetSkeletonBonesRec(pNode->mChildren[i], res, pos, ID, recThroughSkeleton);
		}
	}
	else {
		mat4x4 I;
		mat4x4_identity(I);
		for (GLuint i = 0; i < pNode->mNumChildren; i++) {
			GetSkeletonBonesRec(pNode->mChildren[i], I, pos, ID, recThroughSkeleton);
		}
	}

}


