#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <string>


class Shader {
	public:
		Shader();
		~Shader();
		
		void Unload();
		void Load(const std::string& filename);
		
		GLuint GetProgram();
		void UseProgram();
	private:
		GLuint shaderProgram;
};