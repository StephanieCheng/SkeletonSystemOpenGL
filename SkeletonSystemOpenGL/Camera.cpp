#include "Camera.h"
#include <iostream>
Camera::Camera() {

	position[0] = 0.0f;
	position[1] = 0.0f;
	position[2] = 1.0f;

	lookAt[0] = 0.0f;
	lookAt[1] = 0.0f;
	lookAt[2] = 0.0f;

	upVector[0] = 0.0f;
	upVector[1] = 1.0f;
	upVector[2] = 0.0f;

	cameraPitch = 0.0f;
	cameraYaw = 0.0f;
	y_fov = ZOOM;
	mouseSensitivity = SENSITIVTY;
	movementSpeed = SPEED;

	AspectRatio = 1.0f;
	near = 0.01f;
	far = 100.0f;
}

Camera::~Camera() {
}

void Camera::SetPosition(float x, float y, float z)
{
	position[0] = x;
	position[1] = y;
	position[2] = z;
}

void Camera::GetPosition(vec3 pos)
{
	pos[0] = position[0];
	pos[1] = position[1];
	pos[2] = position[2];
}

void Camera::SetLookAt(float x, float y, float z)
{
	lookAt[0] = x;
	lookAt[0] = y;
	lookAt[0] = z;
}

void Camera::GetLookAt(vec3 look)
{
	look[0] = lookAt[0];
	look[1] = lookAt[1];
	look[2] = lookAt[2];
}

void Camera::SetUpVector(float x, float y, float z)
{
	upVector[0] = x;
	upVector[1] = y;
	upVector[2] = z;
}

void Camera::GetUpVector(vec3 up)
{
	up[0] = upVector[0];
	up[1] = upVector[1];
	up[2] = upVector[2];
}

void Camera::SetForwardVector(float x, float y, float z)
{

	lookAt[0] = position[0] + x;
	lookAt[1] = position[1] + y;
	lookAt[2] = position[2] + z;

}


void Camera::GetForwardVector(vec3 forward)
{
	vec3_sub(forward, lookAt, position);
	//vec3_norm(forward, forward);
}

void Camera::SetYFov(float newFov)
{
	y_fov = newFov;
	CalculateProjectionMatrix();
}

void Camera::SetNearPlane(float n)
{
	near = n;
	CalculateProjectionMatrix();
}

void Camera::SetFarPlane(float f)
{
	far = f;
	CalculateProjectionMatrix();
}

void Camera::SetAspectRatio(float aspect)
{
	AspectRatio = aspect;
	CalculateProjectionMatrix();
}



void Camera::GetRightVector(vec3 right)
{
	vec3 forward;
	GetForwardVector(forward);
	
	vec3_mul_cross(right, forward, upVector);
	vec3_norm(right, right);
}


void Camera::CalculateUpVector()
{
	vec3 right;
	vec3 forward;

	vec3_sub(forward, position, lookAt);
	vec3_norm(forward, forward);
	vec3_mul_cross(right, upVector, forward);
	vec3_norm(right, right);
	vec3_mul_cross(upVector, forward, right);

	//mat4x4_look_at(lookAtMatrix, position, lookAt, upVector);
}

void Camera::GetLookAtMatrix(mat4x4 lookMat)
{
	mat4x4_look_at(lookMat, position, lookAt, upVector);
}

void Camera::GetViewProjectionMatrix(mat4x4 viewproj)
{ 
	mat4x4_dup(viewproj, projectionMatrix);
}

void Camera::CalculateProjectionMatrix()
{
	mat4x4_perspective(projectionMatrix, y_fov, AspectRatio, near, far);
}

void Camera::LookAround(float xoffset, float yoffset)
{

	if (yoffset > 1.57f)
		yoffset = 1.57f;

	if (yoffset < -1.57f)
		yoffset = -1.57f;

	if (cameraPitch + yoffset > 1.5f)
		yoffset = 1.5f - cameraPitch;

	if (cameraPitch + yoffset < -1.5f)
		yoffset = -1.5f - cameraPitch;


	cameraYaw += xoffset;
	cameraPitch += yoffset;

	//std::cout << cameraYaw << " " << cameraPitch << std::endl;


	//std::cout << pitch << " " << yaw << std::endl;

	float lookAtNorm;
	vec3 camForward;
	vec3_sub(camForward, lookAt, position);
	lookAtNorm = vec3_len(camForward);


	vec3 front;
	/*front[0] = cos(cameraPitch) * cos(cameraYaw);
	front[1] = sin(cameraPitch);
	front[2] = cos(cameraPitch) * sin(cameraYaw);*/

	front[0] = cos(cameraPitch) * sin(cameraYaw);
	front[1] = sin(cameraPitch);
	front[2] = -cos(cameraPitch) * cos(cameraYaw);

	vec3_norm(front, front);
	vec3_scale(front, front, lookAtNorm);


	//std::cout << "front " << front[0] << " "  << front[1] << " " << front[2] << std::endl;

	SetForwardVector(front[0], front[1], front[2]);

}

void Camera::CameraMove(vec3 movement, GLfloat deltaTime)
{
	vec3 forward;
	GetForwardVector(forward);

	position[0] += movementSpeed * deltaTime  * movement[0];
	position[1] += movementSpeed * deltaTime * movement[1];
	position[2] += movementSpeed * deltaTime * movement[2];

	SetForwardVector(forward[0], forward[1], forward[2]);

	
}

void Camera::OrbitCamera(float yoffset, float xoffset)
{

	if (xoffset > 1.57f)
		xoffset = 1.57f;

	if (xoffset < -1.57f)
		xoffset = -1.57f;

	if (cameraPitch + xoffset > 1.5f)
		xoffset = 1.5f - cameraPitch;

	if (cameraPitch + xoffset < -1.5f)
		xoffset = -1.5f - cameraPitch;

	cameraPitch += xoffset;
	cameraYaw += yoffset;

	vec3 camForward;
	vec3 camRight;

	mat4x4 I;
	mat4x4_identity(I);
	mat4x4 R;

	vec4 oldPosition2;
	vec4 oldPosition;


	oldPosition[0] = position[0];
	oldPosition[1] = position[1];
	oldPosition[2] = position[2];
	oldPosition[3] = 1.0f;

	//std::cout << "lookat	" << lookAt[0] << " " << lookAt[1] << " " << lookAt[2] << std::endl;
	//std::cout << "position	" << position[0] << " " << position[1] << " " << position[2] << std::endl;
	//std::cout << cameraYaw << " " << cameraPitch << std::endl;

	vec3_sub(camForward, lookAt, position);
	vec3_norm(camForward, camForward);

	vec3_mul_cross(camRight, camForward, upVector);
	vec3_mul_cross(upVector, camRight, camForward);
	vec3_norm(upVector, upVector);
	vec3_mul_cross(camRight, camForward, upVector);
	//now we have camRight, this is the rotation vector

	//std::cout << camRight[0] << " " << camRight[1] << " " << camRight[2] << std::endl;

	//rotates matrix around 000!!

	mat4x4_rotate(R, I, camRight[0], camRight[1], camRight[2], xoffset);


	oldPosition[0] -= lookAt[0];
	oldPosition[1] -= lookAt[1];
	oldPosition[2] -= lookAt[2];

	mat4x4_mul_vec4(oldPosition2, R, oldPosition);

	oldPosition2[0] += lookAt[0];
	oldPosition2[1] += lookAt[1];
	oldPosition2[2] += lookAt[2];

	//mat4x4_identity(I);
	//mat4x4_translate(I, lookAt[0], lookAt[1], lookAt[2]);

	//mat4x4_mul_vec4(oldPosition, I, oldPosition2);


	oldPosition[0] = oldPosition2[0];
	oldPosition[1] = oldPosition2[1]; 
	oldPosition[2] = oldPosition2[2];

	position[0] = oldPosition2[0];
	position[1] = oldPosition2[1];
	position[2] = oldPosition2[2];


	//yaxis

	vec3 yaxis = { 0.0f, 1.0f, 0.0f };

	vec3_sub(camForward, lookAt, position);

	vec3_mul_cross(camRight, camForward, yaxis);
	vec3_mul_cross(upVector, camRight, camForward);
	vec3_norm(upVector, upVector);
	vec3_mul_cross(camRight, camForward, upVector);

	mat4x4_rotate_Y(R, I, yoffset);


	oldPosition[0] -= lookAt[0];
	oldPosition[1] -= lookAt[1];
	oldPosition[2] -= lookAt[2];

	mat4x4_mul_vec4(oldPosition2, R, oldPosition);

	oldPosition2[0] += lookAt[0];
	oldPosition2[1] += lookAt[1];
	oldPosition2[2] += lookAt[2];


	position[0] = oldPosition2[0];
	position[1] = oldPosition2[1];
	position[2] = oldPosition2[2];

}

void Camera::PanCamera(float xoffset, float yoffset)
{

	vec3 right;
	GetRightVector(right);

	vec3 forwardVec;
	GetForwardVector(forwardVec);
	vec3_norm(upVector, upVector);
	

	position[0] += right[0] * xoffset;
	position[1] += right[1] * xoffset;
	position[2] += right[2] * xoffset;

	position[0] += upVector[0] * yoffset;
	position[1] += upVector[1] * yoffset;
	position[2] += upVector[2] * yoffset;

	SetForwardVector(forwardVec[0], forwardVec[1], forwardVec[2]);
	
}
