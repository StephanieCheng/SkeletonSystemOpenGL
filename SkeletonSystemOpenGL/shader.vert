#version 330 core
//in is same as attribute for vertex shader
//varying is same as out for vertex shader or same as in for fragment shaders
//varying and attribute are sort of depricated

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords;
layout (location = 3) in ivec4 boneIDs;
layout (location = 4) in vec4 weights;
uniform int animation;
uniform int selectedBone;
uniform int renderType;

out vec2 TexCoords; 
out vec4 WeightColor;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

const int MAX_BONES = 200;

uniform mat4 gBones[MAX_BONES];
void main()
{
	TexCoords = texCoords;
	vec4 newpos = vec4(position,1.0f);
	
	mat4 bonetransform = gBones[boneIDs[0]] * weights[0];
	bonetransform += gBones[boneIDs[1]] * weights[1];
	bonetransform += gBones[boneIDs[2]] * weights[2];
	bonetransform += gBones[boneIDs[3]] * weights[3];
	
	newpos = bonetransform * vec4(position, 1.0f);
	//normals should be also moved by bonetrasform
	
	//vec3 newposition = vec3(position.x * 0.5, position.y * 0.1, position.z * 0.1);
	gl_Position =  projection * view * model * newpos;
	
	if(renderType == 2){
		WeightColor = vec4(0.0f, 0.0f, 1.0f, 1.0f);
		for(int i = 0; i < 4; ++i){
			if(boneIDs[i] == selectedBone){
				WeightColor[0] = weights[i];
				WeightColor[2] = 1.0 - weights[i];
			}
		}
	}	
	
	else if(renderType == 1){
		//render all weights
		WeightColor[0] = 0.0f;
		for(int i = 0; i < 4; ++i){
			if(weights[i] > WeightColor[0]){
				WeightColor[0] = weights[i];
				WeightColor[2] = 1.0f - weights[i];
			}
		}
	}
}