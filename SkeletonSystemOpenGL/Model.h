#pragma once
#include "GL/glew.h"
#include <string>
#include "linmath.h"
#include "Texture.h"
#include <vector>
#include "Mesh.h"

#include <assimp\Importer.hpp>
#include <assimp\scene.h>
#include <assimp\postprocess.h>
#include <map>
#include <chrono>
#include <memory>

typedef std::chrono::high_resolution_clock Time;
typedef std::chrono::milliseconds ms;
typedef std::chrono::duration<float> fsec;

typedef struct BoneInfo {
	mat4x4 boneOffset;
	mat4x4 finalTransformation;
};

typedef struct AnimationInfo {
	Time::time_point tStart;
	Time::time_point tNow;
	int animation;
	float animationTime;
};
typedef struct TransitionInfo {
	int anim1;
	int anim2;
	int frameStart;
	int frameEnd;
	Time::time_point tAnimationStart;
	Time::time_point tAnimationCurrent;
};
typedef struct BlendInfo {
	int anim1;
	int anim2;
	float weight;
	Time::time_point tAnimationStart;
	Time::time_point tAnimationCurrent;
};

class Model {

public:
	Model();
	~Model();

	void LoadAssimp(const std::string& path, bool triangulate);
	void Unload();

	void Draw(Shader& shader);

	std::vector<Texture> GetLoadedTextures();

	const aiScene* scene;


	void SetScale(float x, float y, float z);
	void SetPosition(float x, float y, float z);
	void SetRotation(quat rot);

	void GetModelMatrix(mat4x4 model);

	void BoneTransforms(float timeInSeconds, std::vector<MatStruct>& transforms, bool drawArmature);

	GLuint UsesSkeleton();

	void printMeshBones();

	void PlayAnimation(std::string animName);

	std::map<std::string, int> boneMap;
	std::map<int, BoneInfo> boneInfoMap;
	//std::map<std::string, aiNodeAnim*> animationChannels;

	std::map<std::string, std::map<std::string, aiNodeAnim*> > animations;
	std::vector<TransitionInfo> transitions;
	std::vector<BlendInfo> blends;

	//which animation is played
	GLint animation;
	// whether we are playing animation or just displaying base pose
	bool animate;
	// which blend we are playing (0,1,...) or if its a regular animation (-1)  
	int animateSpecial;
	// animate blend or transitions -- really really hacky way of doing things, 0 - transitions, 1 - blends
	bool animateOther;

	void PlayNextAnimation();
	void StopAnimation();
	void PauseAnimation();
	void UnpauseAnimation();
	void TogglePlayPause();
	std::chrono::milliseconds GetAnimationLengthOfOneFrame();
	void NextFrame();
	void PreviousFrame();
	void ForwardTenFrames();
	void BackwardTenFrames();
	void DrawArmature();

	int FindAnimationIndex(std::string animName);

	void SetMaskAndAnimation(std::string BoneName, std::string AnimationName);
	void ClearMasks();

	int SelectBone(std::string);

	std::vector<Vertex> SkeletonVertiecs;

	void AddBlendWeight(bool sub);

	void MakeBlend(int iAnim1, int iAnim2, float weight);

	void MakeTransitions(int iAnim1, int iAnim2, int frameStart, int frameEnd);
	void ToggleTransitionsOrBlends();
	void TogglePlayAnimBlend();
	void PlayAnimBlend(int i);
	void TurnOffPlayBlend();
	void PlayNextBlend();

	void SetPlayTransition();
	void SetPlayBlend();

private:
	std::vector<Texture> textures_loaded;
	std::vector<std::unique_ptr<Mesh> > meshes;
	std::string directory;

	void processNode(aiNode* node, const aiScene* scene);
	std::unique_ptr<Mesh> processMesh(const aiMesh* mesh, const aiScene* scene);
	std::vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName);


	vec3 position;
	quat rotation;
	vec3 scale;

	mat4x4 modelMatrix;

	void CalculateModelMatrix();

	Time::time_point tAnimationStart;
	Time::time_point tAnimationCurrent;
	bool pause;

	void aiMatTolinMat(mat4x4 mat1, aiMatrix4x4 mat2);

	//keeps ownership of data
	Assimp::Importer importer;

	mat4x4 globalInverseTransformation;
	int numBones;

	void MakeBlend(float animationTime, float animationTime2, const aiNode * pNode, mat4x4 parentTransform, int anim1, int anim2, float factor);

	void ReadNodeHeirarchy(float animationTime, const aiNode* pNode, mat4x4 parentTransform, int animation);

	void CalcInterpolatedRotation(aiQuaternion& out, float animationTime, const aiNodeAnim* pNodeAnim);

	void CalcInterpolatedScaling(aiVector3D& out, float animationTime, const aiNodeAnim* pNodeAnim);

	void CalcIntrepolatedPosition(aiVector3D& out, float animationTime, const aiNodeAnim* pNodeAnim);

	GLuint FindRotation(float animationTime, const aiNodeAnim* pNodeAnim);

	GLuint FindPosition(float animationTime, const aiNodeAnim * pNodeAnim);

	GLuint FindScale(float animationTime, const aiNodeAnim * pNodeAnim);

	GLuint usesSkeleton;

	// masks, assign animations to them
	std::map<std::string, AnimationInfo> BoneMasks;

	SkeletonMesh skeletonMesh;

	void GetSkeletonBones();

	void GetSkeletonBonesRec(aiNode* pNode, mat4x4 parentTransform, vec3 startPosition, int prevID, bool recThroughSkeleton);

	void PlayBlend(float timeInSeconds, std::vector<MatStruct>& transforms, bool drawArmatures);

	void PlayTransition(float timeInSeconds, std::vector<MatStruct>& transforms, bool drawArmatures);

	void GetFinalBoneTransforms(std::vector<MatStruct>& transforms);

};

