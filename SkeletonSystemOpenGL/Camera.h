#pragma once
#include "linmath.h"
#include "GL\glew.h"

const GLfloat YAW = -90.0f;
const GLfloat PITCH = 0.0f;
const GLfloat SPEED = 3.0f;
const GLfloat SENSITIVTY = 0.25f;
const GLfloat ZOOM = 45.0f;



class Camera {
public:
	Camera();
	~Camera();

	void SetPosition(float x,float y,float z);
	void GetPosition(vec3 pos);

	void SetLookAt(float x, float y, float z);
	void GetLookAt(vec3 look);

	void SetUpVector(float x, float y, float z);
	void GetUpVector(vec3 up);

	void SetForwardVector(float x, float y, float z);
	void GetForwardVector(vec3 forward);

	void SetYFov(float newFov);

	void SetNearPlane(float n);
	void SetFarPlane(float f);
	void SetAspectRatio(float aspect);



	void GetRightVector(vec3 right);

	void CalculateUpVector();
	
	void GetLookAtMatrix(mat4x4 lookMat);

	void GetViewProjectionMatrix(mat4x4 viewproj);

	void CalculateProjectionMatrix();

	void CameraMove(vec3 movement, GLfloat deltaTime);

	void LookAround(float xoffset, float yoffset);

	void OrbitCamera(float yoffset, float xoffset);

	void PanCamera(float xoffset, float yoffset);


private:
	vec3 position;
	vec3 lookAt;
	vec3 upVector;

	GLfloat y_fov;
	GLfloat near;
	GLfloat far;
	GLfloat AspectRatio;

	float cameraPitch;
	float cameraYaw;

	mat4x4 projectionMatrix;


	// Camera options
	GLfloat movementSpeed;
	GLfloat mouseSensitivity;


};