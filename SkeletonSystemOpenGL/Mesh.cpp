#include "Mesh.h"
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

Mesh::Mesh()
{

	//vbo = 0;
	//ebo = 0; 
	//vao = 0;

}

Mesh::~Mesh()
{
	std::cout << "mesh deleted" << std::endl;
	//Unload();

}
void Mesh::Unload() {
	/*if (vbo) glDeleteBuffers(1, &vbo);
	if (vao) glDeleteVertexArrays(1, &vao);
	if (ebo) glDeleteBuffers(1, &ebo);

	vbo = 0;
	vao = 0;
	ebo = 0;*/
	for (int i = 0; i < textures.size(); i++) {
		textures[i].Unload();
	}
}

void Mesh::Draw(Shader& shader)
{
	GLuint diffuseNr = 1;
	GLuint specularNr = 1;
	for (GLuint i = 0; i < textures.size(); i++) {
		glActiveTexture(GL_TEXTURE0 + i);

		std::stringstream ss;
		std::string number;
		std::string name = textures[i].GetType();

		if (name == "texture_diffuse")
			ss << diffuseNr++;
		else if (name == "texture_specular")
			ss << specularNr++;
		number = ss.str();


		glUniform1f(glGetUniformLocation(shader.GetProgram(), (name + number).c_str()), i); //"material."
		glBindTexture(GL_TEXTURE_2D, textures[i].GetTexture());
	}
	//glActiveTexture(GL_TEXTURE0);

	//draw
	glBindVertexArray(vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//set stuff to default
	for (GLuint i = 0; i < textures.size(); i++) {
		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
}

GLuint Mesh::GetVertexArray()
{
	return vao;
}

void Mesh::AddBoneData(GLuint boneID, float weight, GLuint vertexID)
{
	if(vbd.size() == 0) vbd.resize(vertices.size());

	for (GLuint i = 0; i < NUM_BONES_PER_VERTEX; i++) {
		if (vbd[vertexID].weights[i] == 0.0f) {
			vbd[vertexID].boneID[i] = boneID;
			vbd[vertexID].weights[i] = weight;
			return;
		}
	}
	//std::cout << "vertex depends on more than 4 bones!! " << boneID << std::endl;
}

bool Mesh::UsesSkeleton()
{
	return usesSkeleton;
}

void Mesh::printVertexBoneData()
{

	std::cout << "num of vertices of mesh " << vbd.size() << std::endl;
	for (int i = 0; i < vbd.size(); i++) {
		std::cout << "vertex " << i << std::endl;

		std::cout << "\tposition " << vertices[i].position[0] << " " << vertices[i].position[1] << " " << vertices[i].position[2] << std::endl;

		//std::cout << "\t" << bones[i].weights[0] << " " << bones[i].weights[1] << std::endl;

		if (vbd[i].weights[0] < 0.1f && vbd[i].weights[1] < 0.1f) {
			std::cout << "\tHAS NO WIEGHTS" << std::endl;
			std::cout << "\t" << vbd[i].weights[0] << " " << vbd[i].weights[1] << std::endl;
		}
		
		for (int j = 0; j < 2; j++) {
			if (vbd[i].boneID[j] == 0) {
				std::cout << "\tbone 0 weigth " << vbd[i].weights[j] << std::endl;
				break; //0 are also not assigned bones only the first 0 is a real bone
			}
		}
		for (int j = 0; j < 2; j++) {
			if (vbd[i].boneID[j] == 1) {
				std::cout << "\tbone 1 weigth " << vbd[i].weights[j] << std::endl;
				break;
			}
		}
		

	}
}

void Mesh::setupMesh(std::vector<Vertex> vertices, std::vector<GLuint> indices, std::vector<Texture> textures)
{
	this->vertices = vertices;
	this->indices = indices;
	this->textures = textures;


	glGenBuffers(1, &ebo); //vbo should be named ebo!
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);

	glBindVertexArray(vao);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, normal));

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, texcoord));

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
}

void Mesh::assignSkeleton() {
	usesSkeleton = true;
	
	//bones.resize(vertices.size());

	//printVertexBoneData();

	glBindVertexArray(vao);

	glGenBuffers(1, &boneVB);
	glBindBuffer(GL_ARRAY_BUFFER, boneVB);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vbd[0]) * vbd.size(), &vbd[0], GL_STATIC_DRAW);
	//bone IDs
	glEnableVertexAttribArray(3);
	glVertexAttribIPointer(3, 4, GL_INT, sizeof(VertexBoneData), (const GLvoid*)0);
	//weights
	glEnableVertexAttribArray(4);
	glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(VertexBoneData), (const GLvoid*)16);
	//glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(VertexBoneData), (const GLvoid*)offsetof(VertexBoneData, weights));

	glBindVertexArray(0);
}

void Mesh::SetDataEBO(GLfloat * vertices, GLuint * indices,GLsizei sizeofvertices, GLsizei sizeofindices)
{
	glGenBuffers(1, &ebo); //vbo should be named ebo!
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);

	glBindVertexArray(vao);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeofvertices, vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeofindices, indices, GL_STATIC_DRAW);
	
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*) (3 * sizeof(GLfloat)));

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

}



SkeletonMesh::SkeletonMesh() {}

SkeletonMesh::~SkeletonMesh() {
	std::cout << "armature deleted" << std::endl;
}

void SkeletonMesh::AddBoneData(GLint prevBoneID, GLuint BoneID, vec3 pos, vec3 pos2)
{
	//bone head belongs to the previous bones transforms, tail belongs to current
	VertexBoneData bd;
	bd.boneID[0] = prevBoneID;
	bd.weights[0] = 1.0f;
	//if this is the root, it has no previous bone
	if (prevBoneID == -1) {
		//bd.weights[0] = 0.0f;
		bd.boneID[0] = BoneID;
	}
	bones.push_back(bd);

	//add the position
	positions.push_back(pos[0]);
	positions.push_back(pos[1]);
	positions.push_back(pos[2]);

	VertexBoneData bd2;
	bd2.boneID[0] = BoneID;
	bd2.weights[0] = 1.0f;

	bones.push_back(bd2);

	//should aslo add end position
	//temp end position is pos + 1.0y

	positions.push_back(pos2[0]);
	positions.push_back(pos2[1]);
	positions.push_back(pos2[2]);

	//index of poisitons and index of bones corresponds and is the vertexID
	
	return;
}

void SkeletonMesh::assignToBuffers()
{
	//glGenBuffers(1, &ebo); //vbo should be named ebo!
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);

	glBindVertexArray(vao);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, positions.size() * sizeof(GLfloat), &positions[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &boneVB);
	glBindBuffer(GL_ARRAY_BUFFER, boneVB);
	glBufferData(GL_ARRAY_BUFFER, sizeof(bones[0]) * bones.size(), &bones[0], GL_STATIC_DRAW);
	//bone IDs
	glEnableVertexAttribArray(1);
	glVertexAttribIPointer(1, 1, GL_INT, sizeof(VertexBoneData), (const GLvoid*)0);
	//weights
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(VertexBoneData), (const GLvoid*)16);

	glBindVertexArray(0);
}

void SkeletonMesh::Draw()
{
	glDisable(GL_DEPTH_TEST);
	//draw
	glEnable(GL_PROGRAM_POINT_SIZE);
	//glEnable(GL_POINT_SMOOTH);
	//glEnable(GL_LINE_SMOOTH);
	glBindVertexArray(vao);
	glDrawArrays(GL_LINES, 0, positions.size()/2);
	glDrawArrays(GL_POINTS, 0, positions.size());
	glBindVertexArray(0);
}
